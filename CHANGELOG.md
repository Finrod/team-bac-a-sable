# CHANGELOG

## v1.2.3
 * 📱 Display a message on Facebook inApp browser
 * 🐛 Fix duplicate route on login
 * 🎨 Add active class on current plan
 * 🐛 Fix click on clock icon

## v1.2.2
 * ⏫ Update dependencies
 * 🎨 Reduce padding on list items
 * 🐛 Fix error when creating user

## v1.2.1
 * 🐛 Fix missing getters

## v1.2.0
 * 📝 Update documentation
 * ♻️ Remove unnecessary getters
 * ✨ Add quotes page
 * 🐛 Fix workbox migration to v4
 * 📱 Display a third line on high resolution in home page
 * 🚸 Improve visibility of the floating button when there are no data
 * 🐛 Add missing alt attribute
 * 🚸 Set default category to CAF
 * ⏫ Update dependencies
 * ✨ Add minima page
 * ♻️ Refactor menu items
 * 👽 Add admin requests for minima
 * ✨ Add admin routes for minima
 * ✨ Add competition deletion from admin page
 * 🚸 Update sort UI in athletes list
 * ✨ Add a sort mode in users list to show next birthday
 * ⏫ Update dependencies
 * ✅ Add new ESLint rules
 * 🎨 Reduce plan form height
 * 🚸 Update athlete screens
 * ✨ Add a script to get athlete data from FFA profile
 * 🚚 Rename resources folder
 * ✨ Add competition update and improve competition page
 * ✨ Add competition page
 * 🐛 Fix sidebar not scrolling
 * ✨ Add competitions list and form
 * ✨ Add PhotoSwipe component
 * 👷 Decrease the number of chuncks
 * ⏫ Update dependencies
 * ✨ Add deleteAllPlans button
 * ✨ Add administration route
 * 🏗 Create constants for collections and images folders
 * ✅ Add prettier
 * 🐛 Fix PWA support for nightly builds
 * ⏫ Update dependencies
 * 👽 Update firebase configuration
 * 🎨 Add nightly visual style
 * 👷 Update CI for nightly builds

## v1.1.0
 * 🤣 New quotes
 * 🚸 Add an "Another Quote!" button
 * ✨ Add paces API and calculation
 * ✨ Update paces values
 * 🐛 Fix missing styles from Bootstrap-Vue
 * 🐛 Fix missing step attribute on input of type number
 * ✨ Add paces screen
 * 🎨 Decrease navbar shadow
 * ⏫ Update dependencies

## v1.0.2
 * 🍏 Fix iOS icons and splash screen
 * ✅ Add a name on each Vue component
 * 📈 Add sentry to log Javascript errors
 * ⏫ Update dependencies
 * 👷 Update CI build
 * 🎨 Add pointer cursor on update banner
 * 🐛 Fix serviceWorker not defined in private mode

## v1.0.1
 * 🐛 Fix athlete results url
 * ✏️ Fix license typo

## v1.0.0
 * 📈 Update browserlist stats
 * 🐛 Fix migration script
 * 🥚 Display citation when no data
 * 📱 Display navBar leftIcon on big screens
 * ✨ Add plan management
 * 🐛 Prevent user to change its own role
 * 🐛 Fix file require in MdViewer
 * 🚸 Get data when role change in case of reload on athlete page
 * 🚸 Add ffaId and licence badges in athletes list
 * ✨ Add marks and maxes to profile for coach role
 * ✨ Add user edit modal
 * 🐛 Fix user creation in database
 * ✨ Manage roles in store
 * ✨ Add pole-vault type
 * 👷 Update CI build
 * 🎨 Change avatar to material avatar style
 * ⏫ Update dependencies
 * 👽 Add entry in database after login for new user Close #61
 * 🚸 Add enter action on login and register
 * 🐛 Fix logout exceptions
 * 🐛 Fix spinner color props
 * ✅ Code quality
 * 🐛 Fix migration script not importing all auth users
 * 📱 Adapt UI to bigger screens
 * ⏫ Update dependencies
 * ✨ Add list of users
 * ✨ Add profile page
 * 👷 Add license field in migration script
 * 🐛 Fix update notes deleting other userData
 * ✅ Code quality
 * 🚸 Add spinner on loading
 * 🐛 Fix JS error on logout
 * ⏫ Update dependencies
 * 🔥 Remove Facebook link
 * 🐛 Fix sidebar not openning
 * 🎨 Change ui icons to use icofont icons
 * ♻️ Use firestore instead of firebase database
 * 👷 Add migration script for firebase db to firestore
 * 🐛 Fix docs build
 * ✅ Add stylelint
 * 🎨 Replace font-awesome with material icons
 * 👷 Optimize build
 * 🚚 Reorganize files
 * ⏫ Update dependencies

## v0.4.5
 * 👟 Add cycle 5
 * 🐛 Fix application update
 * ⏫ Update dependencies

## v0.4.4
 * 🏆 Add Jo Menut results
 * ♻️ Refactor update mechanics
 * 👟 Fix Christmas cycle

## v0.4.3
 * 👟 Add Christmas cycle

## v0.4.2
 * 🚸 Add clic to reload application
 * 🏆 Add challenge Jo Menut 2018

## v0.4.1
 * 🚸 Add link to login on confirmation page
 * 👟 Add trainings plan for cycle 4

## v0.4.0
 * 📝 Add online documentation
 * ✨ Add notes
 * ⏫ Update dependencies

## v0.3.1
 * 0.3.1
 * ⏫ Update dependencies
 * 👟 Add training plans for cycle 3
 * 🐛 Fix bad error message on wrong password error
 * 🚸 Replace delete button by an icon
 * 🚸 Allow text on mark fields
 * 🐛 Fix regression on edit marks
 * 🚸 Preselect jump on form
 * 🐛 Fix jump tab wrapping
 * 🐛 Fix form wrapping
 * 🎨 Add a shadow under the navbar
 * 🚸 Add scrolling only on percentage table
 * ✨ Calculate up to 120%
 * 🚸 Add help text on calculator
 * 🐛 Fix empty marks displaying a /
 * ⏫ Update dependencies

## v0.3.0
 * 0.3.0
 * 📝 Update news
 * ✨ Add max weight management
 * ✨ Add weight calculator
 * 🔊 Add google analytics
 * 🎨 Fix PWA theme color
 * 🐌 Remove cache for index and service worker
 * ✨ Add marks management
 * ⏫ Update dependencies
 * Add htaccess
 * Add marks list items
 * Add buttons in marks page
 * ⏫ Update dependencies, remove vuex-persist
 * 👟 Add trainings plan for cycle 2
 * 👟 Add trainings plan for cycle 1
 * Improve register/connection forms
 * ⏫ Update dependencies
 * Add login, logout and register functions
 * ⏫ Update dependencies
 * 📰 Update news

## v0.2.0
 * 0.2.0
 * Add back button
 * Add news page
 * Add MdViewer component
 * 🐛 Fix update notification position
 * Rework home and navigation
 * Rework update notification
 * ⏫ Update dependencies
 * 🤖 Update .gitlab-ci to always test but deploy only on tags
 * 👟 Add new training plan
 * 🎨 Rework training plans
 * 🎨 Update logo and colors
 * 🎨 Add logo ressources
 * ⏫ Update dependencies
 * 🤖 Add GitLab CI

## v0.1.0
 * 🐛 Fix installation prompt
 * Add update message
 * Add plan view
 * Change icon and fix manifest
 * Rework navigation
 * Add squeleton, menu, home and plans
 * Init empty application
 * Initial commit
