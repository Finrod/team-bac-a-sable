module.exports = {
  root: true,
  env: {
    node: true
  },
  // Global variables: http://eslint.org/docs/user-guide/configuring#specifying-globals
  globals: {
    _VERSION: true
  },
  extends: ['plugin:vue/recommended', '@vue/prettier'],
  rules: {
    // Custom rules from uncategorized category
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    'vue/match-component-file-name': [
      'error',
      {
        extensions: ['jsx', 'js', 'vue'],
        shouldMatchCase: true
      }
    ],
    // allow debug during development
    'no-console': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
