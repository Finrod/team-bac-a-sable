import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import configuration from '@/configuration/config'
import firebasePlugin from '@/api'
import '@/registerServiceWorker'
import '@/filters'

import { mapGetters } from 'vuex'

import VueAnalytics from 'vue-analytics'
import firebase from 'firebase/app'
import 'firebase/auth'

import Icon from '@/components/ui/Icon'

// Sentry for crashlytics
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

// Style
import '@/scss/theme.scss'

// Components
Vue.component('icon', Icon)

// Is production?
const isProd = process.env.NODE_ENV === 'production'

// Plugins
Vue.use(firebasePlugin)
Vue.use(VueAnalytics, {
  id: configuration.analytics,
  router,
  debug: {
    sendHitTask: isProd
  }
})

if (isProd) {
  Sentry.init({
    dsn: 'https://1a36a74ba0164d3f814f42aa51c554e7@sentry.io/1458965',
    release: 'team-bac-a-sable@' + _VERSION,
    integrations: [new Integrations.Vue({ Vue, attachProps: true })]
  })
}

// Use mixin to define common getters to ALL components
// /!\ Use it CAREFULLY as it affect ALL components, including third party plugins /!\
Vue.mixin({
  // mix the getters into computed with object spread operator
  computed: { ...mapGetters(['isRole']) }
})

// Install prompt
if (isProd) {
  window.addEventListener('beforeinstallprompt', eventPromise => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    eventPromise.preventDefault()
    // Show the prompt
    eventPromise.prompt()
  })
}

Vue.config.productionTip = false

// Start app when firebase is initialized
firebase.auth().onAuthStateChanged(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})
