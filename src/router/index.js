import Vue from 'vue'
import Router from 'vue-router'

import store from '@/store'

import Login from '@/components/views/home/Login'
import Register from '@/components/views/home/Register'
import Validate from '@/components/views/home/Validate'
import Home from '@/components/views/home/Home'

const Athletes = () =>
  import(/* webpackChunkName: "v-main" */ '@/components/views/athletes/Athletes')
const Athlete = () => import(/* webpackChunkName: "v-main" */ '@/components/views/athletes/Athlete')
const Body = () => import(/* webpackChunkName: "v-main" */ '@/components/views/body/Body')
const Calculator = () =>
  import(/* webpackChunkName: "v-main" */ '@/components/views/body/Calculator')
const Competitions = () =>
  import(/* webpackChunkName: "v-main" */ '@/components/views/competitions/Competitions')
const Competition = () =>
  import(/* webpackChunkName: "v-main" */ '@/components/views/competitions/Competition')
const Marks = () => import(/* webpackChunkName: "v-main" */ '@/components/views/marks/Marks')
const Minima = () => import(/* webpackChunkName: "v-main" */ '@/components/views/minima/Minima')
const Paces = () => import(/* webpackChunkName: "v-main" */ '@/components/views/paces/Paces')
const Plans = () => import(/* webpackChunkName: "v-main" */ '@/components/views/plans/Plans')
const Plan = () => import(/* webpackChunkName: "v-main" */ '@/components/views/plans/Plan')
const Quotes = () => import(/* webpackChunkName: "v-main" */ '@/components/views/quotes/Quotes')
const Notes = () => import(/* webpackChunkName: "v-main" */ '@/components/views/notes/Notes')
// const JoMenut2018 = () => import(/* webpackChunkName: "v-challenges" */ '@/components/views/challenges/JoMenut2018')

const Admin = () => import(/* webpackChunkName: "v-admin" */ '@/components/views/admin/Admin')
const AdminMinima = () =>
  import(/* webpackChunkName: "v-admin" */ '@/components/views/admin/Minima')

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: Home,
      meta: {
        auth: true
      }
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/validate',
      component: Validate
    },
    {
      path: '/plans/:id',
      component: Plan,
      props: true,
      meta: {
        auth: true
      }
    },
    {
      path: '/plans',
      component: Plans,
      meta: {
        auth: true
      }
    },
    {
      path: '/paces',
      component: Paces,
      meta: {
        auth: true
      }
    },
    {
      path: '/body',
      component: Body,
      meta: {
        auth: true
      }
    },
    {
      path: '/calculator',
      component: Calculator,
      meta: {
        auth: true
      }
    },
    {
      path: '/marks',
      component: Marks,
      meta: {
        auth: true
      }
    },
    {
      path: '/competitions',
      component: Competitions,
      meta: {
        auth: true
      }
    },
    {
      path: '/competitions/:id',
      component: Competition,
      props: true,
      meta: {
        auth: true
      }
    },
    {
      path: '/minima',
      component: Minima,
      meta: {
        auth: true
      }
    },
    {
      path: '/quotes',
      component: Quotes,
      meta: {
        auth: true
      }
    },
    {
      path: '/notes',
      component: Notes,
      meta: {
        auth: true
      }
      // }, {
      //   path: '/joMenut2018',
      //   component: JoMenut2018,
      //   meta: {
      //     auth: true
      //   }
    },
    {
      path: '/athletes',
      component: Athletes,
      meta: {
        auth: true
      }
    },
    {
      path: '/athletes/:uid',
      component: Athlete,
      props: true,
      meta: {
        auth: true
      }
    },
    {
      path: '/admin',
      component: Admin,
      meta: {
        auth: true
      }
    },
    {
      path: '/admin/minima/:type',
      component: AdminMinima,
      props: true,
      meta: {
        auth: true
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (!store.state.user) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else if (!store.state.user.emailVerified) {
      next({
        path: '/validate',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
