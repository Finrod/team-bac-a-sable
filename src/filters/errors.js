const errors = value => {
  if (!value.code) return value

  switch (value.code) {
    case 'auth/invalid-email':
      return "L'email n'est pas valide."
    case 'auth/user-not-found':
      return 'Cet email ne correspond à aucun utilisateur.'
    case 'auth/email-already-in-use':
      return 'Un compte existe déjà avec cet email.'
    case 'auth/weak-password':
      return 'Le mot de passe doit contenir au moins 6 caractères.'
    case 'auth/wrong-password':
      return 'Impossible de se connecter avec ce mot de passe.'
    default:
      if (process.env.NODE_ENV === 'development') console.warn(value.code)
      return 'Erreur inconnue, veuillez réessayer plus tard.'
  }
}

export default errors
