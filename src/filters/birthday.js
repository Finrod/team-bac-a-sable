import { differenceInCalendarYears, isValid, format } from 'date-fns'
import { fr } from 'date-fns/locale'

/**
 * Return a string telling when is the next birthday
 *
 * @param {object} user the user with birthday, nextBirthday and daysLeftBeforeBirthday attributes
 *
 * @return {string} string of the next birthday
 */
const date = user => {
  if (!user.nextBirthday && !user.daysLeftBeforeBirthday) return ''

  // Return raw date if invalid
  if (!isValid(user.nextBirthday)) return ''

  // Create birthday Date object
  let day = user.birthday.split('/')[0]
  let month = user.birthday.split('/')[1]
  let year = user.birthday.split('/')[2]
  const birthday = new Date(year, month, day)

  // Calculate next age
  const age = differenceInCalendarYears(user.nextBirthday, birthday)

  if (user.daysLeftBeforeBirthday === 0) {
    return `${age} ans ajourd'hui ! Bon anniversaire !!! 🎉`
  } else if (user.daysLeftBeforeBirthday === 1) {
    return `${age} ans demain !`
  } else if (user.daysLeftBeforeBirthday <= 10) {
    return `${age} ans dans ${user.daysLeftBeforeBirthday} jours...`
  } else {
    const nextBirthDayFormatted = format(user.nextBirthday, 'd MMMM', { locale: fr })
    return `${age} ans le ${nextBirthDayFormatted}`
  }
}

export default date
