import Vue from 'vue'

import birthday from './birthday'
import date from './date'
import errors from './errors'

Vue.filter('birthday', birthday)
Vue.filter('date', date)
Vue.filter('errors', errors)
