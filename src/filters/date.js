import { format, isValid } from 'date-fns'

/**
 * Return the locale formatted string of a date from an epoch in milliseconds
 *
 * @param {integer} epoch       date in milliseconds
 * @param {string}  formatStr   date format [optional, default 'full'] {full|date|hour}
 * @param {boolean} utc         true=format to UTC date, false=format to local date
 *
 * @return {string} string of the date
 */
const date = (epoch, formatStr = 'dd/MM/yyyy') => {
  if (!epoch) return ''

  let d = new Date(parseInt(epoch))

  // Return raw date if invalid
  if (!isValid(d)) return epoch

  return format(d, formatStr)
}

export default date
