import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  // Navbar
  back: false,
  leftButton: false,
  title: 'Team Bac à Sable',
  // Common data
  plans: [],
  competitions: [],
  quotes: [],
  // Authenticated user
  user: null,
  // Authenticated user data
  userData: {},
  userMarks: [],
  userBodyMaxes: []
}

export const getters = {
  isRole: state => {
    return {
      admin: state.userData.role === 'admin',
      coach: state.userData.role === 'coach',
      user: state.userData.role === 'user'
    }
  }
}

const mutations = {
  setBack(state, back) {
    state.back = back
  },

  setLeftButton(state, leftButton) {
    state.leftButton = leftButton
  },

  setTitle(state, title) {
    state.title = title
  },

  setPlans(state, plans) {
    state.plans = []
    plans.forEach(doc => {
      state.plans.push({ id: doc.id, ...doc.data() })
    })
  },

  setCompetitions(state, competitions) {
    state.competitions = []
    competitions.forEach(doc => {
      state.competitions.push({ id: doc.id, ...doc.data() })
    })
  },

  setQuotes(state, quotes) {
    state.quotes = []
    quotes.forEach(doc => {
      state.quotes.push({ id: doc.id, ...doc.data() })
    })
  },

  setUser(state, user) {
    state.user = user
  },

  setUserData(state, userData) {
    state.userData = userData
  },

  setUserMarks(state, userMarks) {
    state.userMarks = []
    userMarks.forEach(doc => {
      const mark = { id: doc.id, ...doc.data() }
      state.userMarks.push(mark)
    })
  },

  setUserBody(state, userBody) {
    state.userBodyMaxes = []
    userBody.forEach(doc => {
      const max = { id: doc.id, ...doc.data() }
      state.userBodyMaxes.push(max)
    })
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations
})
