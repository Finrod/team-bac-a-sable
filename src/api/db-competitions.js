import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

import config from '@/configuration/config'

export default {
  addCompetition: async (competition, file) => {
    // Upload file if needed
    if (file) {
      await firebase
        .storage()
        .ref()
        .child(competition.schedule)
        .put(file)
    }

    // Save in database
    firebase
      .firestore()
      .collection(config.db.collections.competitions)
      .add(competition)
  },

  getCompetitionFile: fileref => {
    return firebase
      .storage()
      .ref()
      .child(fileref)
      .getDownloadURL()
  },

  patchCompetition: async (competitionId, competition, file) => {
    // Update file if needed
    if (file) {
      await firebase
        .storage()
        .ref()
        .child(competition.schedule)
        .put(file)
    }

    firebase
      .firestore()
      .collection(config.db.collections.competitions)
      .doc(competitionId)
      .set(competition)
  },

  deleteCompetition: competition => {
    // Remove file
    if (competition.schedule) {
      firebase
        .storage()
        .ref()
        .child(competition.schedule)
        .delete()
    }

    // Remove data
    firebase
      .firestore()
      .collection(config.db.collections.competitions)
      .doc(competition.id)
      .delete()
  }
}
