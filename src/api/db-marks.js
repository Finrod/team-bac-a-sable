import firebase from 'firebase/app'
import 'firebase/firestore'
import { find, orderBy, without } from 'lodash-es'

import config from '@/configuration/config'
import store from '@/store'

export default {
  getMarks: async userId => {
    const snapshot = await firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.marks)
      .get()

    const marks = []
    snapshot.forEach(doc => {
      const m = doc.data()
      m.id = doc.id
      // Convert stride to number to be able to sort it
      m.strides = Number(m.strides)
      // Remove empty marks
      m.marks = without(m.marks, '')

      m.label = find(config.jumps, ['code', m.jump]).label
      marks.push(m)
    })

    return orderBy(marks, ['label', 'strides', 'preDash'], ['asc', 'desc', 'desc'])
  },

  addMark: mark => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.marks)
      .add(mark)
  },

  patchMark: (markId, mark) => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.marks)
      .doc(markId)
      .set(mark)
  },

  deleteMark: markId => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.marks)
      .doc(markId)
      .delete()
  }
}
