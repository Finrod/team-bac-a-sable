import firebase from 'firebase/app'
import 'firebase/firestore'

import config from '@/configuration/config'

export default {
  getMinimaByType: async (type = 'outdoor') => {
    const snapshot = await firebase
      .firestore()
      .collection(config.db.collections.minima)
      .where('type', '==', type)
      .get()

    const minima = []
    snapshot.forEach(doc => {
      minima.push(doc.data())
    })

    return minima
  },

  getMinimaByTypeAndCategory: async (type = 'outdoor', cat = 'CAF') => {
    const snapshot = await firebase
      .firestore()
      .collection(config.db.collections.minima)
      .where('type', '==', type)
      .where('category', '==', cat)
      .get()

    const minima = []
    snapshot.forEach(doc => {
      minima.push(doc.data())
    })

    return minima
  },

  patchMinima: minima => {
    const db = firebase.firestore()

    // Get a new write batch
    const batch = db.batch()

    // Add all minima
    for (const m of minima) {
      const mRef = db
        .collection(config.db.collections.minima)
        .doc(`${m.type}-${m.competition}-${m.category}-${m.event}`)
      batch.set(mRef, m)
    }

    // Commit the batch
    return batch.commit()
  }
}
