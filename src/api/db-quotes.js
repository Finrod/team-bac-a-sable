import firebase from 'firebase/app'
import 'firebase/firestore'

import config from '@/configuration/config'

export default {
  addQuote: quote => {
    firebase
      .firestore()
      .collection(config.db.collections.quotes)
      .add(quote)
  },

  patchQuote: (quoteId, quote) => {
    firebase
      .firestore()
      .collection(config.db.collections.quotes)
      .doc(quoteId)
      .set(quote)
  },

  deleteQuote: quoteId => {
    firebase
      .firestore()
      .collection(config.db.collections.quotes)
      .doc(quoteId)
      .delete()
  }
}
