import firebase from 'firebase/app'
import 'firebase/auth'

import store from '@/store'

export default {
  createUserWithEmailAndPassword: async (email, password) => {
    await firebase.auth().createUserWithEmailAndPassword(email, password)
  },

  updateProfile: async (displayName, photoURL) => {
    await store.state.user.updateProfile({ displayName, photoURL })
  },

  sendEmailVerification: async () => {
    await store.state.user.sendEmailVerification()
  },

  signInWithEmailAndPassword: async (email, password) => {
    await firebase.auth().signInWithEmailAndPassword(email, password)
  },

  logout: async () => {
    await firebase.auth().signOut()
  }
}
