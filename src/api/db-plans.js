import firebase from 'firebase/app'
import 'firebase/firestore'

import config from '@/configuration/config'

export default {
  addPlan: plan => {
    firebase
      .firestore()
      .collection(config.db.collections.plans)
      .add(plan)
  },

  patchPlan: (planId, plan) => {
    firebase
      .firestore()
      .collection(config.db.collections.plans)
      .doc(planId)
      .set(plan)
  },

  deletePlan: planId => {
    firebase
      .firestore()
      .collection(config.db.collections.plans)
      .doc(planId)
      .delete()
  }
}
