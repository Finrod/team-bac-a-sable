import body from './db-body'
import competitions from './db-competitions'
import marks from './db-marks'
import minima from './db-minima'
import notes from './db-notes'
import paces from './db-paces'
import plans from './db-plans'
import quotes from './db-quotes'
import users from './db-users'

export default {
  ...body,
  ...competitions,
  ...marks,
  ...minima,
  ...notes,
  ...paces,
  ...plans,
  ...quotes,
  ...users
}
