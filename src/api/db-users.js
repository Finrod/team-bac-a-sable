import firebase from 'firebase/app'
import 'firebase/firestore'
import { filter } from 'lodash-es'
import config from '@/configuration/config'

export default {
  getUsers: async () => {
    const snapshot = await firebase
      .firestore()
      .collection(config.db.collections.users)
      .get()

    const users = []
    snapshot.forEach(doc => {
      const user = doc.data()
      user.id = doc.id
      users.push(user)
    })

    // Remove test user in production
    let filterEmail = ''
    if (process.env.NODE_ENV === 'production') filterEmail = 'finrod927@gmail.com'
    return filter(users, u => u.email !== filterEmail)
  },

  getUser: async uid => {
    const user = await firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(uid)
      .get()
    if (user.exists) return user.data()
    else return { firstName: 'John Doe', license: 'Cet athlète est inconnu...' }
  },

  // Add user after sign in
  addUser: async user => {
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(user.uid)
      .set({
        firstName: user.displayName,
        email: user.email,
        license: '',
        ffaId: '',
        role: 'user',
        notes: ''
      })
  },

  patchUser: async (userId, user) => {
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .update(user)
  }
}
