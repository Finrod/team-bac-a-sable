import firebase from 'firebase/app'
import 'firebase/firestore'

import config from '@/configuration/config'
import store from '@/store'

export default {
  patchPaces: (v30ml, vma) => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .update({ v30ml, vma })
  }
}
