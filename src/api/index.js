import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import * as Sentry from '@sentry/browser'

import auth from './auth'
import db from './db'

import store from '@/store'
import config from '@/configuration/config'

export default {
  install: Vue => {
    // Initialize Firebase
    firebase.initializeApp(config.firebase)
    firebase.auth().languageCode = 'fr'

    // Update user on change
    firebase.auth().onAuthStateChanged(async user => {
      const database = firebase.firestore()

      // Save user auth data
      store.commit('setUser', user)

      // user is null when logging out, so don't get data...
      if (!user) return

      // onError callback for loggued out users
      const onError = () => {}

      // Get user data
      let userDb = database.collection(config.db.collections.users).doc(user.uid)
      const userData = await userDb.get()

      // If not in database, add it
      if (!userData.exists) await db.addUser(user)

      // Set user on sentry
      Sentry.configureScope(scope => {
        scope.setUser({ id: user.uid, username: user.displayName })
      })

      userDb.onSnapshot(doc => store.commit('setUserData', doc.data()), onError)
      userDb
        .collection(config.db.collections.marks)
        .onSnapshot(col => store.commit('setUserMarks', col), onError)
      userDb
        .collection(config.db.collections.bodyMaxes)
        .onSnapshot(col => store.commit('setUserBody', col), onError)

      database
        .collection(config.db.collections.plans)
        .onSnapshot(col => store.commit('setPlans', col), onError)

      database
        .collection(config.db.collections.competitions)
        .onSnapshot(col => store.commit('setCompetitions', col), onError)

      database
        .collection(config.db.collections.quotes)
        .onSnapshot(col => store.commit('setQuotes', col), onError)
    })

    // Vue plugin with auth functions
    Vue.prototype.$auth = auth

    // Vue plugin with database functions
    Vue.prototype.$db = db
  }
}
