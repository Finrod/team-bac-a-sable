import firebase from 'firebase/app'
import 'firebase/firestore'
import { find, orderBy } from 'lodash-es'

import config from '@/configuration/config'
import store from '@/store'

export default {
  getBodyMaxes: async userId => {
    const snapshot = await firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.bodyMaxes)
      .get()

    const bodyMaxes = []
    snapshot.forEach(doc => {
      const bm = doc.data()
      bm.id = doc.id
      bm.label = find(config.exercices, ['code', bm.exercice]).label
      bodyMaxes.push(bm)
    })

    return orderBy(bodyMaxes, ['label'])
  },

  addBodyMax: bodyMax => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.bodyMaxes)
      .add(bodyMax)
  },

  patchBodyMax: (bodyMaxId, bodyMax) => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.bodyMaxes)
      .doc(bodyMaxId)
      .set(bodyMax)
  },

  deleteBodyMax: bodyMaxId => {
    const userId = store.state.user.uid
    firebase
      .firestore()
      .collection(config.db.collections.users)
      .doc(userId)
      .collection(config.db.collections.bodyMaxes)
      .doc(bodyMaxId)
      .delete()
  }
}
