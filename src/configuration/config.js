export default {
  firebase: {
    apiKey: 'AIzaSyDvb96QJApQC0KPlbsxHr-noZKkF39Cfrg',
    authDomain: 'team-bac-a-sable.firebaseapp.com',
    databaseURL: 'https://team-bac-a-sable.firebaseio.com',
    projectId: 'team-bac-a-sable',
    storageBucket: 'team-bac-a-sable.appspot.com',
    messagingSenderId: '72346661117',
    appId: '1:72346661117:web:0f8752af9f4baf93'
  },

  analytics: 'UA-127727381-1',

  db: {
    collections: {
      users: 'users',
      bodyMaxes: 'bodyMaxes',
      marks: 'marks',
      plans: 'plans',
      competitions: 'competitions',
      minima: 'minima',
      quotes: 'quotes'
    }
  },

  menu: [
    // home: 1-always displayed, 2-displayed on high resolutions, false: not displayed (only sidebar)
    { route: '/plans', label: 'Séances', icon: 'list', auth: true, home: 1 },
    { route: '/paces', label: 'Allures', icon: 'runner-alt-1', auth: true, home: 1 },
    { route: '/body', label: 'Musculation', icon: 'muscle-weight', auth: true, home: 1 },
    { route: '/competitions', label: 'Calendrier', icon: 'ui-calendar', auth: true, home: 1 },
    { route: '/minima', label: 'Minima', icon: 'ticket', auth: true, home: 2 },
    { route: '/marks', label: 'Marques', icon: 'foot-print', auth: true, home: 1 },
    { route: '/athletes', label: 'Athlètes', icon: 'users-alt-3', auth: true, home: 2 },
    { route: '/quotes', label: 'Perles', icon: 'speech-comments', auth: true, home: 2 },
    { route: '/notes', label: 'Notes', icon: 'ui-note', auth: true, home: 1 }
    // { route: '/joMenut2018', label: 'Jo Menut', icon: 'trophy', auth: true }
  ],

  jumps: [
    { code: 'lj', label: 'Longueur' },
    { code: 'tj', label: 'Triple Saut' },
    { code: 'hj', label: 'Hauteur' },
    { code: 'pv', label: 'Perche' }
  ],

  exercices: [
    { code: 'lift', label: 'Arraché' },
    { code: 'bench', label: 'Développé couché' },
    { code: 'press', label: 'Presse' },
    { code: 'raise', label: 'Soulevé de terre' },
    { code: 'squat', label: 'Squat' }
  ],

  paces: {
    v30ml: {
      distances: [70, 80, 90, 100, 110, 120, 150],
      intencities: [85, 90, 95]
    },
    vma: {
      distances: [100, 200, 250, 300, 400, 500],
      intencities: [90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160]
    }
  },

  minima: {
    categories: ['CAF', 'CAM', 'JUF', 'JUM', 'ESF', 'ESM', 'SEF', 'SEM'],
    competitions: [
      { code: 'REG', label: 'Régionaux' },
      { code: 'PRE', label: 'Pré-France' },
      { code: 'NAT', label: 'France' }
    ],
    events: [
      { code: '60', label: '60m', indoor: true, outdoor: false },
      { code: '100', label: '100m', indoor: false, outdoor: true },
      { code: '200', label: '200m', indoor: true, outdoor: true },
      { code: '60h', label: '60m haies', indoor: true, outdoor: false },
      { code: '100h', label: '100m haies', indoor: false, outdoor: true },
      { code: '110h', label: '110m haies', indoor: false, outdoor: true },
      { code: 'lj', label: 'Longueur', indoor: true, outdoor: true },
      { code: 'tj', label: 'Triple Saut', indoor: true, outdoor: true }
    ]
  }
}
