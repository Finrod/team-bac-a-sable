/* eslint-disable no-console */

import { register } from 'register-service-worker'
import eventBus from '@/eventBus'

if (process.env.NODE_ENV === 'production' && navigator.serviceWorker) {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      console.log('Service worker is active.')
      eventBus.$emit('sw-ready')
    },

    cached() {
      console.log('Content has been cached for offline use.')
      eventBus.$emit('sw-cached')
    },

    updated(registration) {
      console.log('New content is available; please refresh.')
      eventBus.$emit('sw-updated', registration.waiting)
    },

    offline() {
      console.log('No internet connection found. App is running in offline mode.')
      eventBus.$emit('sw-offline')
    },

    error(error) {
      console.error('Error during service worker registration:', error)
      eventBus.$emit('sw-error', error)
    }
  })

  let refreshing
  navigator.serviceWorker.addEventListener('controllerchange', () => {
    if (refreshing) return
    window.location.reload()
    refreshing = true
  })
}
