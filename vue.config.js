const webpack = require('webpack')
const packageJson = require('./package.json')

const isNightly = process.env.VUE_APP_NIGHTLY === 'true'

module.exports = {
  lintOnSave: false,

  css: {
    sourceMap: true
  },

  publicPath: isNightly ? '/team-bac-a-sable/' : '/',

  pwa: {
    name: isNightly ? 'TBAS👻' : 'Team Bac à Sable',
    themeColor: '#efea3a',
    appleMobileWebAppCapable: 'yes',
    iconPaths: {
      appleTouchIcon: 'img/icons/apple-touch-icon-180x180.png',
      msTileImage: 'img/icons/mstile-150x150.png'
    },
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  },

  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.md$/,
          use: 'raw-loader'
        }
      ]
    },

    /**
     * Plugins
     * Reference: http://webpack.github.io/docs/configuration.html#plugins
     * List: http://webpack.github.io/docs/list-of-plugins.html
     */
    plugins: [
      // Define global variables at COMPILATION TIME that can be loaded in code
      new webpack.DefinePlugin({
        _VERSION: JSON.stringify(packageJson.version)
      })
    ]
  }
}
