module.exports = {
  plugins: ['stylelint-scss'],
  extends: [
    'stylelint-config-recess-order',
    'stylelint-config-recommended-scss',
    'stylelint-prettier/recommended'
  ],
  rules: {
    // Give weird errors
    'no-descending-specificity': null
  }
}
