module.exports = {
  port: 8090,
  dest: 'dist/docs',
	base: '/docs/',
	evergreen: true,

	title: 'Team Bac à Sable',
	description: 'L\'application officielle de la Team Bac à Sable',
	head: [
		['link', { rel: 'icon', href: '../favicon.ico' }]
	],

	themeConfig: {
		search: false,
		nav: [
			{ text: 'Accueil', link: '/' },
			{ text: 'Installation', link: '/install.html' },
			{ text: 'Nouveautés', link: '/news.html' }
		]
	},

  postcss: {
    plugins: [ require('autoprefixer')({ browsers: [ 'defaults' ] }) ]
  }
}
