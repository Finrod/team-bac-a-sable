# Installation

- [Android](#automatique) (automatique)
- [Android](#manuelle) (manuelle)
- [iPhone](#iphone)

## Android

### Automatique

Avec votre téléphone, aller sur le site [tbas.finrod.info](https://tbas.finrod.info)

Au bout de quelques secondes, une popup vous invitera à ajouter le site à votre écran d'accueil. Cliquez sur le bouton, laisser faire et voilà, votre application préférée est installée !

Il vous suffit désormais de la lancer à partir de la liste de vos application comme n'importe quelle application de votre téléphone.

### Manuelle

Avec votre téléphone, aller sur le site [tbas.finrod.info](https://tbas.finrod.info)

Si au bout de quelques secondes, vous n'avez pas de popup vous invitant à ajouter le site à votre écran d'accueil, pas de panique ! Aller dans le menu de votre navigateur (les trois points en haut à droite pour Chrome) et cliquez sur "ajouter à votre écran d'accueil", laisser faire et voilà, votre application préférée est installée !

Il vous suffit désormais de la lancer à partir de la liste de vos application comme n'importe quelle application de votre téléphone.

## iPhone

Avec votre téléphone, aller sur le site [tbas.finrod.info](https://tbas.finrod.info)

Aller dans le menu de Safari et cliquez sur "ajouter à votre écran d'accueil", laisser faire et voilà, votre application préférée est installée !

Il vous suffit désormais de la lancer à partir de la liste de vos application comme n'importe quelle application de votre téléphone.
