---
pageClass: news
---

# Nouveautés

## Version 1.2.0
> 5 novembre 2019

- ✨ Ajout d'une page d'administration
- ✨ Ajout du calendrier des compétitions, avec affichage des horaires
- ✨ Ajout des minima des championnats (Bretagne, pré-France et France)
- ✨ Ajout d'une page regroupant les perles du groupe
- ✨ Affichage des prochains anniversaires dans la liste des athlètes
- 🚸 Amélioration de la visibilité du bouton d'ajout / édition lorsqu'il n'y a pas de donnée affichée
- 🚸 Mise à jour da fiche d'un athlète
- 📱 Affichage d'une 3e ligne d'icone si l'écran est assez grand
- 🐛 Quelques corrections de bugs

## Version 1.1.0
> 27 août 2019

- ✨ Ajout des allures de courses

## Version 1.0.0
> 16 avril 2019

- 📱 Interface adaptée pour une utilisation sur PC
- ✨ Ajout du type de perche
- ✨ Gestion des plans "à la volée"
- ✨ Gestion des roles des utilisateurs (utilisateur, coach, administrateur)
- ✨ Ajout de la liste des utilisateurs
- ✨ Ajout d'une page de profil

## Version 0.4.0
> 18 novembre 2018

- ✨ Gestion des notes
- ✨ Documentation (aide et nouveautés)

## Version 0.3.1
> 4 novembre 2018

- 🐛 Correction de bugs
- 🚸 Amélioration de l'affichage
- ✨ Possibilité d'utiliser du texte pour les marques
- ✨ Présélection du saut dans le formulaire en fonction des marques affichées
- ✨ Calculatrice de musculation jusqu'à 120%

## Version 0.3.0
> 19 octobre 2018

- ✨ Gestion des comptes utilisateurs
- ✨ Gestion des marques
- ✨ Gestion des max en musculation avec table de calcul

## Version 0.2.0
> 30 juin 2018

- 🚸 Amélioration de la navigation
- 🎨 Nouveau logo et nouvelles couleurs

## Version 0.1.0
> 4 juin 2018

- 🎉 Première version
- ✨ Ajout des plans d'entrainements
