---
home: true
heroImage: /logo.png
actionText: Installation ▶️
actionLink: /install.html
features:
- title: 👣 Marques
  details: Sauvegardez vos marques pour ne plus vous creuser à la tête avant chaque compétition !
- title: 💪 Musculation
  details: Combien font 35% de 65kg ? La calculatrice intégrée le fait pour vous à partir de vos max !
- title: 🍬 Notes
  details: Un paris à noter ? Un résultat à sauvegarder ? Notez tout ça dans un coin pour ne pas l'oublier !
---
