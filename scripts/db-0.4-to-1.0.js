/* eslint-disable */
console.log('Data already migrated!')
return

// Get data to migrate
const db04 = require('./db-0.4.json')
const firebase = require('firebase')
// Required for side-effects
require('firebase/firestore')

// Init firebase
firebase.initializeApp({
  apiKey: 'AIzaSyDvb96QJApQC0KPlbsxHr-noZKkF39Cfrg',
  authDomain: 'team-bac-a-sable.firebaseapp.com',
  databaseURL: 'https://team-bac-a-sable.firebaseio.com',
  projectId: 'team-bac-a-sable',
  storageBucket: 'team-bac-a-sable.appspot.com',
  messagingSenderId: '72346661117'
})
const db = firebase.firestore()

// Admin access to firebase
const admin = require('firebase-admin')
const serviceAccount = require('./serviceAccountKey.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://team-bac-a-sable.firebaseio.com'
})

// ============= FUNCTIONS =============
const getUsers = async () => {
  const users = {}
  return admin.auth().listUsers()
    .then(listUsersResult => {
      listUsersResult.users.forEach(userRecord => {
        users[userRecord.uid] = {
          firstName: userRecord.displayName,
          email: userRecord.email
        }
      })
      return Promise.resolve(users)
    })
    .catch(error => {
      console.log('Error getting users:', error)
      return Promise.reject(error)
    })
}

const compileData = async () => {
  // Get users
  const users = await getUsers()

  // Add data from database to users list
  for (const userId in users) {
    const user = users[userId]
    user.notes = db04[userId] ? db04[userId].notes : ''
    user.bodyMaxes = db04.bodyMaxes[userId] || {}
    user.marks = db04.marks[userId] || {}
  }

  // console.log(JSON.stringify(users, null, 2))
  return Promise.resolve(users)
}

const migrateData = async () => {
  console.log(`\n⚠️ Do not forget to clear database before importing new data!\n`)

  const usersData = await compileData()

  // process.exit()

  for (const userId in usersData) {
    const user = usersData[userId]
    console.log(`\nImporting user ${user.firstName}...`)

    await db.collection('users').doc(userId)
      .set({
        firstName: user.firstName,
        email: user.email,
        license: '',
        ffaId: '',
        role: 'user',
        notes: user.notes || ''
      })
      .then(docRef => { console.log('  ✔️ Properties') })
      .catch(error => { console.error('  ❌ Properties\n', error) })

    // bodyMaxes
    if (user.bodyMaxes) {
      for (const bm in user.bodyMaxes) {
        await db.collection('users').doc(userId).collection('bodyMaxes')
          .add(user.bodyMaxes[bm])
          .then(docRef => { console.log('  ✔️ Body max') })
          .catch(error => { console.error('  ❌ Body max\n', error) })
      }
    }

    // marks
    if (user.marks) {
      for (const m in user.marks) {
        const mark = user.marks[m]
        mark.pvType = ''

        await db.collection('users').doc(userId).collection('marks')
          .add(mark)
          .then(docRef => { console.log('  ✔️ Marks') })
          .catch(error => { console.error('  ❌ Marks\n', error) })
      }
    }
  }

  console.log('\nAll done!')
  process.exit()
}

migrateData()
