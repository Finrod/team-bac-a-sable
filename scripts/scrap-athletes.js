const puppeteer = require('puppeteer')

// Admin access to firebase
const admin = require('firebase-admin')
const serviceAccount = require('./serviceAccountKey.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://team-bac-a-sable.firebaseio.com'
})
const db = admin.firestore()

scrapAthletes()

// ============= FUNCTIONS =============
async function scrapAthletes() {
  const baseUrl = 'https://bases.athle.fr/asp.net/athletes.aspx?base=resultats&saison=2019&seq='
  const users = await getUsers()

  for (let i = 0; i < users.length; i++) {
    const user = users[i]
    console.log(`Scrapping data for ${user.firstName}...`)

    if (!user.ffaId) {
      console.log(`⚠️ User ${user.firstName} skipped, no FFA ID for this user!`)
      continue
    }

    const data = await scrapAthlete(`${baseUrl}${user.ffaId}`)
    await updateUser(user, data)
  }

  process.exit()
}

async function getUsers() {
  const snapshot = await db.collection('users').get()

  const users = []
  snapshot.forEach(doc => {
    const user = doc.data()
    user.id = doc.id
    users.push(user)
  })

  return users
}

async function scrapAthlete(url) {
  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()
  await page.setViewport({ width: 1920, height: 926 })
  await page.goto(url)

  // get athlete details
  let athleteData = await page.evaluate(() => {
    let json = {}
    const dataTable = document.querySelectorAll('#ctnContentDetails > table table')[0]

    json.birthday = dataTable.querySelector('tr:nth-child(1) td:nth-child(3)').innerText

    const category = dataTable.querySelector('tr:nth-child(4) td:nth-child(3)').innerText
    json.category = category.split('/')[0]
    json.gender = category.split('/')[1]

    const license = dataTable.querySelector('tr:nth-child(6) td:nth-child(3)').innerText
    json.license = license.split(' - ')[0]

    return json
  })

  await browser.close()
  return athleteData
}

async function updateUser(user, data) {
  await db
    .collection('users')
    .doc(user.id)
    .update(data)
    .then(() => {
      console.log(`✔️ User ${user.firstName} updated!`)
    })
    .catch(e => {
      console.log(`❌ Error when updating ${user.firstName}: ${e}`)
    })
}
